//
//  RepositoriesViewController.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage

class RepositoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let identifuCell = "repository_cell"
    let repositoryRequester = RepositoryRequester()
    
    var page = 1
    var isBusy = false
    var isTheEnd = false
    var repositories = [Repository]()
    
    @IBOutlet weak var tableRepositories: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadRepositories()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (sender!.isKindOfClass(RepositoryCell)){
            let repositoryViewController = segue.destinationViewController as! RepositoryDetailsViewController
            let repositoryCell = sender as! RepositoryCell
            repositoryViewController.repository = repositoryCell.repository
        }
    }
    
    func loadRepositories (){
        HUD.show(.Progress)
        isBusy = true
        repositoryRequester.getRepositories(page) { (repositories) in
            if repositories.count == 0 {
                self.isTheEnd = true
            }else {
                self.repositories += repositories
                self.tableRepositories.reloadData()
            }
            HUD.hide()
            self.isBusy = false
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(identifuCell) as! RepositoryCell
        cell.setRepository(repositories[indexPath.row])
        
        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let currentPosition = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height - (800)
        
        if !isBusy && !isTheEnd && currentPosition > 0 && currentPosition >= contentHeight {
            page += 1
            loadRepositories()
        }
    }
}
