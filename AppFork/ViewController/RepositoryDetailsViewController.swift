//
//  RepositoryDetailsViewController.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import UIKit
import PKHUD

class RepositoryDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let repositoryRequester = RepositoryRequester()
    
    var repository: Repository?
    var pullRequests = [PullRequest]()

    @IBOutlet weak var tablePullRequests: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let repository = repository {
            self.loadRepositoryInformations(repository)
        }
    }

    func loadRepositoryInformations(repository: Repository) {
        self.navigationItem.title = repository.name
        
        HUD.show(.Progress)
        repositoryRequester.getPullRequests(repository) { (pullRequests) in
            self.pullRequests = pullRequests
            self.tablePullRequests.reloadData()
            HUD.hide()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequests.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("pull_identify") as! PullRequestCell
        cell.setPullInformations(pullRequests[indexPath.row])
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequest = pullRequests[indexPath.row]
        if let url = pullRequest.url {
            UIApplication.sharedApplication().openURL(NSURL(string: url)!)
        }
    }
}
