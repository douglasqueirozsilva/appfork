//
//  PullRequestCell.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import UIKit

class PullRequestCell: UITableViewCell {

    @IBOutlet weak var pullTitleLabel: UILabel!
    @IBOutlet weak var pullBodyLabel: UILabel!
    @IBOutlet weak var ownerImageImageView: UIImageView!
    @IBOutlet weak var ownerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setPullInformations(pullRequest: PullRequest) {
        pullTitleLabel.text = pullRequest.title
        pullBodyLabel.text = pullRequest.body
        
        if let owner = pullRequest.owner {
            ownerName.text = owner.name
            
            if let avatarUrl = owner.avatarUrl {
                ownerImageImageView.sd_setImageWithURL(NSURL(string: avatarUrl))
            }
        }
    }
}
