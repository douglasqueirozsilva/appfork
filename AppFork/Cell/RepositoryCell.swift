//
//  RepositoryCell.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import UIKit

class RepositoryCell: UITableViewCell {

    
    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var repositoryDescriptionLabel: UILabel!
    @IBOutlet weak var repositoryForksLabel: UILabel!
    @IBOutlet weak var repositoryStarsLabel: UILabel!
    
    @IBOutlet weak var ownerImageImageView: UIImageView!
    @IBOutlet weak var ownerNameLabel: UILabel!
    
    var repository: Repository?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setRepository(repository: Repository) {
        self.repository = repository
        
        repositoryNameLabel.text = repository.name
        repositoryDescriptionLabel.text = repository.description
        
        if let forks = repository.fork {
            repositoryForksLabel.text = "Forks: \(forks)"
        }
        
        if let stars = repository.start {
            repositoryStarsLabel.text = "Stars: \(stars)"
        }
        
        if  let owner = repository.owner {
            ownerNameLabel.text = owner.name!
            
            if let avatarUrl = owner.avatarUrl {
                ownerImageImageView.sd_setImageWithURL(NSURL(string: avatarUrl))
            }
        }
    }
}
