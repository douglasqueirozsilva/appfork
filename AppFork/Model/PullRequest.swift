//
//  PullRequest.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {
    var owner: Owner?
    var title: String?
    var date: String?
    var body: String?
    var url: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        owner <- map["user"]
        title <- map["title"]
        body <- map["body"]
        url <- map["html_url"]
    }
}