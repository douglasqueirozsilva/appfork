//
//  Owner.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import Foundation
import ObjectMapper

class Owner: Mappable {
    var name: String?
    var avatarUrl: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["login"]
        avatarUrl <- map["avatar_url"]
    }
}