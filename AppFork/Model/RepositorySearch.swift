//
//  RepositorySearch.swift
//  AppFork
//
//  Created by Douglas Queiroz on 8/28/16.
//  Copyright © 2016 Douglas Queiroz. All rights reserved.
//

import Foundation
import ObjectMapper

class RepositorySearch: Mappable {
    var totalCount: Int?
    var incompleteResults: Bool?
    var items: [Repository]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        totalCount <- map["total_count"]
        incompleteResults <- map["incomplete_results"]
        items <- map["items"]
    }
}